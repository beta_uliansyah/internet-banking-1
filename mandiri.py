import socket
import httplib
import ssl
import urllib
import re
from httplib import (
    HTTPConnection,
    HTTPS_PORT,
    )
from sgmllib import SGMLParser
from datetime import date
from common import (
    BaseBrowser,
    FormParser,
    open_file,
    to_float,
    )


# http://askubuntu.com/questions/116020/python-https-requests-urllib2-to-some-sites-fail-on-ubuntu-12-04-without-proxy
class HTTPSConnection(HTTPConnection):
    "This class allows communication via SSL."
    default_port = HTTPS_PORT

    def __init__(self, host, port=None, key_file=None, cert_file=None,
            strict=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT,
            source_address=None):
        HTTPConnection.__init__(self, host, port, strict, timeout,
                source_address)
        self.key_file = key_file
        self.cert_file = cert_file

    def connect(self):
        "Connect to a host on a given (SSL) port."
        sock = socket.create_connection((self.host, self.port),
                self.timeout, self.source_address)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        # this is the only line we modified from the httplib.py file
        # we added the ssl_version variable
        self.sock = ssl.wrap_socket(sock, self.key_file, self.cert_file, ssl_version=ssl.PROTOCOL_TLSv1)

#now we override the one in httplib
httplib.HTTPSConnection = HTTPSConnection
# ssl_version corrections are done


class SaldoParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.data = {} 
        self.data_label = None
        self.value = [] 

    def start_td(self, attrs):
        pass

    def end_td(self):
        if self.value:
            self.data[self.data_label] = ' '.join(self.value)
            self.data_label = None
            self.value = []

    def handle_data(self, data):
        data = data.strip()
        if not data:
            return
        if data in ['Nomor Rekening', 'Jenis Rekening', 'Informasi Saldo']:
            self.data_label = data
            return
        if not self.data_label:
            return
        if data == ':':
            return
        self.value.append(data)

    def get_clean_data(self):
        mata_uang, nominal = self.data['Informasi Saldo'].split()
        return [(
            self.data['Nomor Rekening'],
            self.data['Jenis Rekening'],
            mata_uang.strip('.'),
            to_float(nominal)
            )]


class MutasiParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.hasil = []
        self.baris = []
        self.data = []
        self.last_error = None

    def start_tr(self, attrs):
        pass

    def end_tr(self):
        if self.baris:
            self.hasil.append( self.baris )
        self.baris = []

    def start_td(self, attrs):
        pass

    def end_td(self):
        if self.data:
            self.baris.append(' '.join(self.data))
        self.data = []

    def handle_data(self, data):
        data = data.strip()
        if not data:
            return
        self.data.append(data)

    def get_clean_data(self):
        data = []
        for r in self.hasil:
            if ' '.join(r).find('TIDAK DAPAT DIPROSES') > -1:
                self.last_error = 'Tidak dapat diproses'
                return data
            if r[0] == 'Nomor Rekening':
                rekening = r[2].split()[0]
                continue
            if r[0].find('Saldo Awal') > -1:
                saldo = to_float(r[2])
                continue
            # Cari pola tanggal 10 karakter, 31/03/2009
            if len(r[0]) != 10:
                continue
            match = re.compile(r'([\d][\d])/[\d][\d]/[\d][\d][\d][\d]').search(r[0])
            if not match:
                continue
            d, m, y = map(lambda x: int(x), r[0].split('/'))
            tgl = date(y, m, d)
            ket = r[1]
            if r[2] == '0,00':
                nominal = to_float(r[3])
            else:
                nominal = - to_float(r[2])
            data.append([tgl, ket, nominal])
        # Transaksi terbaru ada di paling atas
        data.reverse()
        result = []
        for tgl, ket, nominal in data:
            saldo += nominal
            result.append([rekening, tgl, ket, nominal, saldo])
        return result 


class Browser(BaseBrowser):
    def __init__(self, username, password, parser, output_file=None):
        super(Browser, self).__init__('https://ib.bankmandiri.co.id',
            username, password, parser, output_file=output_file)

    def login(self):
        self.open_url('/retail/Login.do?action=form&lang=in_ID')
        self.br.select_form(nr=0)
        self.br['userID'] = self.username
        self.br['password'] = self.password
        self.info('Login %s' % self.username)
        resp = self.br.submit(name='image')
        content = resp.read()
        if re.compile('Maaf(.*) blokir').search(content):
            self.last_error = 'Telah diblokir'
        elif re.compile('Anda tidak dapat login').search(content):
            self.last_error = 'Tidak dapat login'
        else:
            return True

    def logout(self):
        self.open_url('/retail/Logout.do?action=result')


class SaldoBrowser(Browser):
    def __init__(self, username, password, output_file=None):
        Browser.__init__(self, username, password, SaldoParser,
                         output_file=output_file)

    def browse(self):
        content = self.get_content('/retail/AccountList.do?action=acclist')
        parser = FormParser()
        parser.feed(content)
        account_id = parser.selects['saSelect']['onchange'].split("'")[1]
        p = {'action': 'result',
             'ACCOUNTID': account_id}
        return self.open_url('/retail/AccountDetail.do', GET_data=p) 

 
class MutasiBrowser(Browser):
    def __init__(self, username, password, output_file=None):
        Browser.__init__(self, username, password, MutasiParser,
                         output_file=output_file)

    def browse(self, tgl):
        day = str(tgl.day)
        month = str(tgl.month)
        year = str(tgl.year)
        content = self.get_content('/retail/TrxHistoryInq.do?action=form')
        parser = FormParser()
        parser.feed(content)
        account_id = parser.selects['fromAccountID']['option'][0]
        self.br.select_form(nr=0)
        self.br['fromAccountID'] = [account_id] 
        self.br['fromDay'] = self.br['toDay'] = [day]
        self.br['fromMonth'] = self.br['toMonth'] = [month]
        self.br['fromYear'] = self.br['toYear'] = [year]
        self.info('Account ID %s' % account_id)
        return self.br.submit()


if __name__ == '__main__':
    import sys
    from optparse import OptionParser
    from pprint import pprint
    from common import to_date
    pars = OptionParser()
    pars.add_option('-u', '--username')
    pars.add_option('-p', '--password')
    pars.add_option('-d', '--date', help='dd-mm-yyyy')
    pars.add_option('', '--mutasi-file')
    pars.add_option('', '--saldo-file')
    pars.add_option('', '--output-file')
    option, remain = pars.parse_args(sys.argv[1:])

    if option.mutasi_file:
        content = open_file(option.mutasi_file)
        parser = MutasiParser()
        parser.feed(content)
        data = parser.get_clean_data()
        pprint(data)
        sys.exit()

    if option.saldo_file:
        content = open_file(option.saldo_file)
        parser = SaldoParser()
        parser.feed(content)
        data = parser.get_clean_data()
        pprint(data)
        sys.exit()

    if not option.username or not option.password:
        print('--username dan --password harus diisi')
        sys.exit()

    if option.date:
        crawler = MutasiBrowser(option.username, option.password,
                                option.output_file)
        tgl = to_date(option.date)
        data = crawler.run(tgl)
    else:
        crawler = SaldoBrowser(option.username, option.password,
                               option.output_file)
        data = crawler.run()
    pprint(data)
