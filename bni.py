import re
from sgmllib import SGMLParser
from datetime import date
from common import (
    BaseBrowser,
    USER_AGENT_MOBILE,
    FormParser,
    open_file,
    to_float as base_to_float
    )


class SaldoParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.data = {} 
        self.data_label = None

    def handle_data(self, data):
        data = data.strip()
        if not data:
            return
        if data in ['Nomor Rekening', 'Produk', 'Mata Uang', 'Saldo Akhir']:
            self.data_label = data
            return
        if self.data_label:
            self.data[self.data_label] = data
            self.data_label = False

    def get_clean_data(self):
        return [(
            self.data['Nomor Rekening'],
            self.data['Produk'],
            self.data['Mata Uang'],
            to_float(self.data['Saldo Akhir'])
            )]


class MutasiParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.hasil = [] 
        self.data = []
        self.baca = {}
        self.baca_data = False
        self.rekening = None
        self.baca_rekening = False

    def start_td(self, attrs):
        self.baca_data = True 

    def end_td(self):
        self.baca_data = False
        if 'Saldo' in self.baca:
            self.data = self.data[:2] + [' '.join(self.data[2:-3])] + \
                        self.data[-3:]
            self.hasil.append(self.data)
            self.data = []
            self.baca = {}

    def handle_data(self, data):
        if not self.baca_data:
            return
        data = data.strip()
        if not data:
            return
        if data == 'Nomor Rekening':
            self.baca_rekening = True
            return
        if self.baca_rekening:
            self.rekening = data
            self.baca_rekening = False
            return
        if data in ['Tanggal Transaksi', 'Uraian', 'Tipe', 'Nominal', 'Saldo']:
            self.baca[data] = True
            return
        if self.baca:
            self.data.append(data)

    def get_clean_data(self):
        data = []
        for tgl, any1, ket, kode, nominal, saldo in self.hasil:
            nominal = nominal.lstrip('IDR ')
            nominal = to_float(nominal)
            if kode.upper() == 'DB': 
                nominal = - nominal
            saldo = to_float(saldo)
            t = tgl.split('-')
            d = int(t[0])
            m = MONTHS.index(t[1].lower())
            y = int(t[2])
            tgl = date(y, m, d)
            data.append([tgl, ket, nominal, saldo])
        # Transaksi terbaru paling atas, perlu dibalik
        data.reverse()
        result = []
        for tgl, ket, nominal, saldo in data:
            result.append([self.rekening, tgl, ket, nominal, saldo])
        return result


def to_float(s):
    return float(base_to_float(s.replace('IDR ', '')))


MONTHS = ['', 'jan', 'feb', 'mar', 'apr', 'may', 'jun',
              'jul', 'aug', 'sep', 'oct', 'nov', 'dec']

ERR_LOGIN = 'User ID atau Password Anda salah'


class Browser(BaseBrowser):
    def __init__(self, username, password, parser, output_file=None):
        super(Browser, self).__init__('https://ibank.bni.co.id',
            username, password, parser, USER_AGENT_MOBILE, output_file)

    def login(self):
        self.open_url()
        resp = self.br.follow_link(text_regex=re.compile('MASUK'))
        self.br.select_form(nr=0)
        self.br['CorpId'] = self.username
        self.br['PassWord'] = self.password
        self.info('Login %s' % self.username)
        resp = self.br.submit(name='__AUTHENTICATE__', label='Login')
        content = resp.read()
        if content.find(ERR_LOGIN) > -1:
            self.last_error = ERR_LOGIN
        else:
            return True
        
    def logout(self):
        self.info('Logout')
        self.br.select_form(nr=0)
        self.br.submit(name='LogOut', label='Keluar')
        self.br.select_form(nr=0)
        self.br.submit(name='__LOGOUT__', label='Keluar')


class SaldoBrowser(Browser):
    def __init__(self, username, password, output_file=None):
        Browser.__init__(self, username, password, SaldoParser, output_file)

    def browse(self):
        self.info('Menu Rekening')
        self.br.follow_link(text_regex=re.compile('REKENING'))
        self.info('Menu Saldo')
        self.br.follow_link(text_regex=re.compile('INFORMASI SALDO'))
        self.br.select_form(nr=0)
        self.br['MAIN_ACCOUNT_TYPE'] = ['OPR']
        self.info('Pilih Tabungan')
        resp = self.br.submit(name='AccountIDSelectRq', label='Lanjut')
        content = resp.read()
        parser = FormParser()
        parser.feed(content)
        rekening = parser.inputs['acc1']
        self.br.select_form(nr=0)
        self.br['acc1'] = [rekening]
        self.info('Gunakan rekening pertama %s' % rekening)
        return self.br.submit(name='BalInqRq', label='Lanjut')


class MutasiBrowser(Browser):
    def __init__(self, username, password, output_file=None):
        Browser.__init__(self, username, password, MutasiParser, output_file)

    def browse(self, tgl): 
        mmm = MONTHS[tgl.month].title()
        tgl = '-'.join([str(tgl.day).zfill(2), mmm, str(tgl.year)])
        self.info('Menu Rekening')
        self.br.follow_link(text_regex=re.compile('REKENING'))
        self.info('Menu Mutasi')
        self.br.follow_link(text_regex=re.compile('MUTASI REKENING'))
        self.info('Pilih Tabungan')
        self.br.select_form(nr=0)
        self.br['MAIN_ACCOUNT_TYPE'] = ['OPR']
        resp = self.br.submit(name='AccountIDSelectRq', label='Lanjut')
        content = resp.read()
        parser = FormParser()
        parser.feed(content)
        rekening = parser.inputs['acc1']
        self.info('Gunakan rekening pertama %s' % rekening)
        self.br.select_form(nr=0)
        self.br['acc1'] = [rekening]
        self.br['Search_Option'] = ['Date']
        self.br['txnSrcFromDate'] = tgl 
        self.br['txnSrcToDate'] = tgl 
        self.info('Mutasi transaksi')
        return self.br.submit(name='FullStmtInqRq', label='Lanjut')


if __name__ == '__main__':
    import sys
    from optparse import OptionParser
    from pprint import pprint
    from common import to_date
    pars = OptionParser()
    pars.add_option('-u', '--username')
    pars.add_option('-p', '--password')
    pars.add_option('-d', '--date', help='dd-mm-yyyy')
    pars.add_option('', '--mutasi-file')
    pars.add_option('', '--saldo-file')
    pars.add_option('', '--output-file')
    option, remain = pars.parse_args(sys.argv[1:])

    if option.mutasi_file:
        content = open_file(option.mutasi_file)
        parser = MutasiParser()
        parser.feed(content)
        pprint(parser.get_clean_data())
        sys.exit()

    if option.saldo_file:
        content = open_file(option.saldo_file)
        parser = SaldoParser()
        parser.feed(content)
        pprint(parser.get_clean_data())
        sys.exit()

    if not option.username or not option.password:
        print('--username dan --password harus diisi')
        sys.exit()

    if option.date:
        crawler = MutasiBrowser(option.username, option.password,
                                option.output_file)
        tgl = to_date(option.date)
        data = crawler.run(tgl)
        pprint(data)
    else:
        crawler = SaldoBrowser(option.username, option.password,
                               option.output_file)
        data = crawler.run()
        pprint(data)
